import { computed, defineComponent, inject, ref, onMounted } from "vue";
export default defineComponent({
  props: {
    block: { typpe: Object },
    formData: { type: Object },
  },
  setup(props) {
    const blockStyles = computed(() => ({
      top: `${props.block.top}px`,
      left: `${props.block.left}px`,
      zIndex: `${props.block.zIndex}`,
    }));
    const config = inject("config");
    const blockRef = ref(null);
    onMounted(() => {
      const width = blockRef.value.offsetWidth;
      const height = blockRef.value.offsetHeight;
      if (props.block.alignCenter) {
        props.block.left = props.block.left - width / 2;
        props.block.top = props.block.top - height / 2;
        props.block.alignCenter = false;
      }
      props.block.width = width;
      props.block.height = height;
    });

    return () => {
      // 通过 block 的 key 属性直接获取对应的组件
      const component = config.componentMap[props.block.key];
      const renderComponent = component.render({
        props: props.block.props,
        model: Object.keys(component.model || {}).reduce((prev, modelName) => {
          let propName = props.block.model[modelName];
          prev[modelName] = {
            modelValue: props.formData[propName],
            "onUpdate:modelValue": (v) => (props.formData[propName] = v),
          };
          return prev;
        }, {}),
      });

      return (
        <div class="editor-block" style={blockStyles.value} ref={blockRef}>
          {renderComponent}
        </div>
      );
    };
  },
});

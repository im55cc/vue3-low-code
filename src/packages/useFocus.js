import { computed, ref } from "vue";
export function useFocus(data, previewRef, callback) {
  let selectIndex = ref(-1);
  const lastSelectBlock = computed(() => {
    return data.value.blocks[selectIndex.value];
  });
  const clearBlockFocus = () => {
    data.value.blocks.map((block) => (block.focus = false));
    selectIndex.value = -1;
  };
  const blockMouseDown = (e, block, index) => {
    if (previewRef.value) return;
    e.preventDefault();
    e.stopPropagation();
    if (e.ctrlKey) {
      if (focusData.value.focus.length <= 1) {
        block.focus = true;
      } else {
        block.focus = !block.focus;
      }
    } else {
      if (!block.focus) {
        clearBlockFocus();
        block.focus = true;
      }
    }
    selectIndex.value = index;
    callback(e);
  };
  const focusData = computed(() => {
    let focus = [];
    let unfocus = [];
    data.value.blocks.forEach((block) => {
      (block.focus ? focus : unfocus).push(block);
    });
    return { focus, unfocus };
  });
  const containerMouseDown = () => {
    clearBlockFocus();
  };
  return {
    focusData,
    lastSelectBlock,
    blockMouseDown,
    containerMouseDown,
    clearBlockFocus,
  };
}

import { reactive } from "vue";
import { events } from "./event";
export function useBlockDragger(focusData, lastSelectBlock, previewRef, data) {
  let dragState = {
    startX: 0,
    startY: 0,
    draging: false,
    startPos: focusData.value.focus.map((block) => ({
      left: block.left,
      top: block.top,
    })),
  };

  const mouseDown = (e) => {
    if (previewRef.value) return;
    const {
      width: BWidth,
      height: BHeight,
      top: BTop,
      left: BLeft,
    } = lastSelectBlock.value; // 拖拽后的最后的元素
    // console.log(BTop, BLeft);
    dragState = {
      startX: e.clientX,
      startY: e.clientY,
      startLeft: BLeft,
      startTop: BTop,
      draging: false,
      startPos: focusData.value.focus.map((block) => ({
        left: block.left,
        top: block.top,
      })),
      lines: (() => {
        const { unfocus } = focusData.value; // 获取其他没选中的以他们的位置做辅助线
        let lines = { x: [], y: [] };
        [
          ...unfocus,
          {
            top: 0,
            left: 0,
            width: data.value.container.width,
            height: data.value.container.height,
          },
        ].forEach((block) => {
          const {
            width: AWidth,
            height: AHeight,
            top: ATop,
            left: ALeft,
          } = block;
          // showTop 表示辅助线的位置，top 表示拖拽元素(B)距离顶部的位置
          // 底对顶
          lines.y.push({ showTop: ATop, top: ATop - BHeight });
          // 中对中
          lines.y.push({ showTop: ATop, top: ATop }); // 顶对顶
          lines.y.push({
            showTop: ATop + AHeight / 2,
            top: ATop + AHeight / 2 - BHeight / 2,
          });
          //底对底
          lines.y.push({
            showTop: ATop + AHeight,
            top: ATop + AHeight - BHeight,
          });
          //顶对底
          lines.y.push({ showTop: ATop + AHeight, top: ATop + AHeight });

          lines.x.push({ showLeft: ALeft, left: ALeft - BWidth });
          lines.x.push({ showLeft: ALeft, left: ALeft });
          lines.x.push({
            showLeft: ALeft + AWidth / 2,
            left: ALeft + AWidth / 2 - BWidth / 2,
          });
          lines.x.push({
            showLeft: ALeft + AWidth,
            left: ALeft + AWidth - BWidth,
          });
          lines.x.push({ showLeft: ALeft + AWidth, left: ALeft + AWidth });
        });
        return lines;
      })(),
    };
    // console.table(dragState);
    document.addEventListener("mousemove", mouseMove);
    document.addEventListener("mouseup", mouseUp);
  };
  let markLine = reactive({ x: null, y: null });
  const mouseMove = (e) => {
    if (!dragState.draging) {
      dragState.draging = true;
      events.emit("start");
    }
    // 计算当前元素最新的 left 和 top 去线里找，找到显示线
    // 鼠标移动后 - 鼠标移动前 + left
    let durX = e.clientX - dragState.startX;
    let durY = e.clientY - dragState.startY;
    let left = dragState.startLeft + durX;
    let top = dragState.startTop + durY;
    // 计算横线 距离参照物元素还有 5 像素的时候，就显示这条线
    let x = null;
    let y = null;
    for (let i = 0; i < dragState.lines.y.length; i++) {
      const { showTop: s, top: t } = dragState.lines.y[i];
      if (Math.abs(t - top) <= 5) {
        durY = t - dragState.startTop;
        y = s;
        break;
      } else {
        durY = e.clientY - dragState.startY;
      }
    }
    // 计算纵线
    for (let i = 0; i < dragState.lines.x.length; i++) {
      const { showLeft: s, left: l } = dragState.lines.x[i];
      if (Math.abs(l - left) <= 5) {
        durX = l - dragState.startLeft;
        x = s;
        break;
      } else {
        durX = e.clientX - dragState.startX;
      }
    }
    focusData.value.focus.forEach((block, index) => {
      block.left = dragState.startPos[index].left + durX;
      block.top = dragState.startPos[index].top + durY;
    });
    markLine.y = y;
    markLine.x = x;
  };
  const mouseUp = (e) => {
    document.removeEventListener("mousemove", mouseMove);
    document.removeEventListener("mouseup", mouseUp);
    markLine.y = null;
    markLine.x = null;
    if (dragState.draging) {
      dragState.draging = false;
      events.emit("end");
    }
  };
  return { mouseDown, markLine };
}

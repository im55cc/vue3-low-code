import deepcopy from "deepcopy";
import { onUnmounted } from "vue";
import { events } from "./event";

export function useCommand(data, focusData) {
  const state = {
    current: -1,
    queue: [],
    commands: {},
    commandArr: [],
    destroyArr: [],
  };
  const registry = (command) => {
    state.commandArr.push(command);
    state.commands[command.name] = (...args) => {
      const { redo, undo } = command.execute(...args);
      redo();
      if (!command.pushQueue) return; //不需要放到队列里直接 return
      let { queue } = state;
      // 如果先放了1 -> 2 -> 撤回 -> 3
      // 1->3
      if (queue.length > 0) {
        queue = queue.slice(0, state.current + 1); // 可能放置的过程中有撤销操作、所以根据最新的 current 值来计算队列
        state.queue = queue;
      }
      queue.push({ redo, undo }); // 保存指令的前进、后退
      state.current = state.current + 1;
    };
  };
  registry({
    name: "redo",
    keyBoard: "ctrl+y",
    execute() {
      return {
        redo() {
          let item = state.queue[state.current + 1]; // 找到下一步
          if (item) {
            item.redo && item.redo();
            state.current++;
          }
        },
      };
    },
  });
  registry({
    name: "undo",
    keyBoard: "ctrl+z",
    execute() {
      return {
        redo() {
          if (state.current == -1) return;
          let item = state.queue[state.current];
          if (item) {
            item.undo && item.undo();
            state.current--;
          }
        },
      };
    },
  });
  registry({
    name: "drag",
    pushQueue: true,
    init() {
      // 初始化操作
      this.before = null;
      // 监控拖拽开始，保存状态
      const start = () => {
        this.before = deepcopy(data.value.blocks);
      };
      // 拖拽之后需要触发对应的指令
      const end = () => {
        state.commands.drag();
      };
      events.on("start", start);
      events.on("end", end);
      return () => {
        events.off("start", start);
        events.off("end", end);
      };
    },
    execute() {
      let before = this.before;
      let after = data.value.blocks;
      return {
        redo() {
          data.value = { ...data.value, blocks: after };
        },
        undo() {
          // 前一步的
          data.value = { ...data.value, blocks: before };
        },
      };
    },
  });
  registry({
    name: "updateContainer", // 更新整个容器
    pushQueue: true,
    execute: (newValue) => {
      const state = {
        before: data.value,
        after: newValue,
      };
      return {
        redo: () => {
          data.value = state.after;
        },
        undo: () => {
          data.value = state.before;
        },
      };
    },
  });
  registry({
    name: "placeTop",
    pushQueue: true,
    execute() {
      const before = deepcopy(data.value.blocks);
      return {
        redo() {
          const { focus, unfocus } = focusData.value;
          const maxZIndex = unfocus.reduce((prev, block) => {
            return Math.max(prev, block.zIndex);
          }, -Infinity);
          focus.forEach((block) => (block.zIndex = maxZIndex + 1)); // 让当前选中的比最大的+1
        },
        undo() {
          data.value = { ...data.value, blocks: before };
        },
      };
    },
  });
  registry({
    name: "placeBottom",
    pushQueue: true,
    execute() {
      const before = deepcopy(data.value.blocks);
      return {
        redo() {
          const { focus, unfocus } = focusData.value;
          let minZIndex =
            unfocus.reduce((prev, block) => {
              return Math.min(prev, block.zIndex);
            }, Infinity) - 1;
          if (minZIndex < 0) {
            unfocus.forEach((block) => (block.zIndex += Math.abs(minZIndex)));
            minZIndex = 0;
          }
          focus.forEach((block) => (block.zIndex = minZIndex)); // 让当前选中的比最大的+1
        },
        undo() {
          data.value = { ...data.value, blocks: before };
        },
      };
    },
  });
  registry({
    name: "delete",
    pushQueue: true,
    execute() {
      let before = deepcopy(data.value.blocks);
      return {
        redo() {
          const { unfocus } = focusData.value;
          data.value = { ...data.value, blocks: unfocus };
        },
        undo() {
          data.value = { ...data.value, blocks: before };
        },
      };
    },
  });
  registry({
    name: "updateBlock", // 更新某个组件
    pushQueue: true,
    execute(newBlock, oldBlock) {
      let state = {
        before: data.value.blocks,
        after: (() => {
          let blocks = [...data.value.blocks]; // 拷贝一分用于新的 block
          const index = data.value.blocks.indexOf(oldBlock); // 找老的，需要通过老的查找
          if (index > -1) {
            blocks.splice(index, 1, newBlock);
          }
          return blocks;
        })(),
      };
      return {
        redo: () => {
          data.value = { ...data.value, blocks: state.after };
        },
        undo: () => {
          data.value = { ...data.value, blocks: state.before };
        },
      };
    },
  });
  const keyBoardEvent = (() => {
    const onKeydown = (e) => {
      const { ctrlKey, key } = e;
      let keyString = [];
      if (ctrlKey) keyString.push("ctrl");
      keyString.push(key);
      keyString = keyString.join("+");
      state.commandArr.forEach(({ keyBoard, name }) => {
        if (!keyBoard) return;
        if (keyString === keyBoard) {
          state.commands[name]();
          e.preventDefault();
        }
      });
    };
    const init = () => {
      // 初始化函数
      window.addEventListener("keydown", onKeydown);
      return () => {
        // 销毁函数
        window.removeEventListener("keydown", onKeydown);
      };
    };
    return init;
  })();
  (() => {
    state.destroyArr.push(keyBoardEvent());
    state.commandArr.forEach((command) => {
      command.init && state.destroyArr.push(command.init());
    });
  })();
  onUnmounted(() => {
    state.destroyArr.forEach(fn && fn());
  });
  return state;
}

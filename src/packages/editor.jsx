import { computed, defineComponent, inject, ref } from "vue";
import EditorBlock from "./editor-block";
import EditorOperator from "./editor-operator";
import deepcopy from "deepcopy";
import { $dialog } from "../components/dialog";
import { $dropDown, dropDownItem } from "../components/dropDown";
import "./editor.scss";
import { useMenuDragger } from "./useMenuDragger";
import { useFocus } from "./useFocus";
import { useBlockDragger } from "./useBlockDragger";
import {
  DArrowLeft,
  DArrowRight,
  SoldOut,
  Sell,
  SortDown,
  SortUp,
  Delete,
  Iphone,
  Edit,
} from "@element-plus/icons-vue";
import { useCommand } from "./useCommand";
export default defineComponent({
  props: {
    modelValue: { type: Object },
    formData: { type: Object },
  },
  emits: ["update:modelValue"],
  setup(props, ctx) {
    const data = computed({
      get() {
        return props.modelValue;
      },
      set(newValue) {
        ctx.emit("update:modelValue", deepcopy(newValue));
      },
    });
    const containerStyles = computed(() => ({
      width: data.value.container.width + "px",
      height: data.value.container.height + "px",
    }));
    const containerRef = ref(null);
    const previewRef = ref(false);
    const config = inject("config");
    // 1.实现菜单的拖拽功能
    const { dragStart, dragEnd } = useMenuDragger(data, containerRef);
    // 2.获取焦点
    const {
      blockMouseDown,
      containerMouseDown,
      clearBlockFocus,
      focusData,
      lastSelectBlock,
    } = useFocus(data, previewRef, (e) => {
      // 获取焦点后实现组件拖拽
      mouseDown(e);
    });
    // 3.实现组件拖拽
    const { mouseDown, markLine } = useBlockDragger(
      focusData,
      lastSelectBlock,
      previewRef,
      data
    );
    const { commands } = useCommand(data, focusData);
    const buttons = [
      {
        label: "撤销",
        icon: <DArrowLeft />,
        handler: () => {
          commands.undo();
        },
      },
      {
        label: "重做",
        icon: <DArrowRight />,
        handler: () => {
          commands.redo();
        },
      },
      {
        label: "导出",
        icon: <Sell />,
        handler: () => {
          $dialog({
            title: "导出 JSON ",
            content: JSON.stringify(data.value),
          });
        },
      },
      {
        label: "导入",
        icon: <SoldOut />,
        handler: () => {
          $dialog({
            title: "导入 JSON",
            content: "",
            onConfirm: (text) => {
              // data.value = JSON.parse(text); // 无法保留历史记录
              commands.updateContainer(JSON.parse(text));
            },
            footer: true,
          });
        },
      },
      {
        label: "置顶",
        icon: <SortUp />,
        handler: () => {
          commands.placeTop();
        },
      },
      {
        label: "置底",
        icon: <SortDown />,
        handler: () => {
          commands.placeBottom();
        },
      },
      {
        label: "删除",
        icon: <Delete />,
        handler: () => {
          commands.delete();
        },
      },
      {
        label: () => (previewRef.value ? "编辑" : "预览"),
        icon: () => (previewRef.value ? <Edit /> : <Iphone />),
        handler: () => {
          previewRef.value = !previewRef.value;
          clearBlockFocus();
        },
      },
    ];
    // 鼠标右击事件
    const onContextMenuBlock = (e, block) => {
      e.preventDefault();
      $dropDown({
        el: e.target,
        content: () => {
          return (
            <>
              <dropDownItem
                label="删除"
                icon="9999"
                onClick={() => commands.delete()}
              ></dropDownItem>
            </>
          );
        },
      });
    };
    return () => (
      <div class="editor">
        <div class="editor-left">
          {/* 根据注册列表 渲染对应的内容 */}
          {config.componentList.map((component) => (
            <div
              class="editor-left-item"
              draggable
              onDragstart={(e) => dragStart(e, component)}
              onDragend={dragEnd}
            >
              <span>{component.label}</span>
              <div>{component.preview()}</div>
            </div>
          ))}
        </div>
        <div class="editor-top">
          {buttons.map((button) => {
            const icon =
              typeof button.icon === "function" ? button.icon() : button.icon;
            const label =
              typeof button.label === "function"
                ? button.label()
                : button.label;
            return (
              <div class="editor-top-button" onClick={button.handler}>
                <el-icon size="20">{icon}</el-icon>
                <span>{label}</span>
              </div>
            );
          })}
        </div>
        <div class="editor-right">
          <EditorOperator
            block={lastSelectBlock.value}
            data={data.value}
            updateContainer={commands.updateContainer}
            updateBlock={commands.updateBlock}
          ></EditorOperator>
        </div>
        <div class="editor-container">
          {/* 负责产生滚动条 */}
          <div class="editor-container-canvas">
            {/* 产生内容区域 */}
            <div
              class="editor-container-canvas__content"
              style={containerStyles.value}
              ref={containerRef}
              onMousedown={containerMouseDown}
            >
              {data.value.blocks.map((block, index) => (
                <EditorBlock
                  class={[
                    block.focus ? "editor-block-focus" : "",
                    previewRef.value ? "editor-block-preview" : "",
                  ]}
                  block={block}
                  formData={props.formData}
                  onMousedown={(e) => blockMouseDown(e, block, index)}
                  onContextmenu={(e) => onContextMenuBlock(e, block)}
                ></EditorBlock>
              ))}
              {markLine.y !== null && (
                <div class="line-x" style={{ top: markLine.y + "px" }}></div>
              )}
              {markLine.x !== null && (
                <div class="line-y" style={{ left: markLine.x + "px" }}></div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  },
});

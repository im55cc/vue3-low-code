import { events } from "./event";

export const useMenuDragger = (data, containerRef) => {
  let currentComponent = null;
  const dragStart = (e, component) => {
    // dragenter 进入元素中 添加一个移动标识
    // dragover 在目标元素经过，必须要组织默认行为，否则不能出发 drop
    // dragleave 离开元素的时候，需要增加一个禁用标识
    // drop 松手的时候根据拖拽的组件 添加一个组件
    containerRef.value.addEventListener("dragenter", dragEnter);
    containerRef.value.addEventListener("dragover", dragOver);
    containerRef.value.addEventListener("dragleave", dragLeave);
    containerRef.value.addEventListener("drop", drop);
    currentComponent = component;
    events.emit("start"); // 发布start
  };
  const dragEnd = () => {
    containerRef.value.removeEventListener("dragenter", dragEnter);
    containerRef.value.removeEventListener("dragover", dragOver);
    containerRef.value.removeEventListener("dragleave", dragLeave);
    containerRef.value.removeEventListener("dragend", dragEnd);
    containerRef.value.removeEventListener("drop", drop);
    events.emit("end"); // 发布end
  };
  const dragEnter = (e) => {
    e.dataTransfer.dropEffect = "move";
  };
  const dragOver = (e) => {
    e.preventDefault();
  };
  const dragLeave = (e) => {
    e.dataTransfer.dropEffect = "none";
  };
  const drop = (e) => {
    const blocks = data.value.blocks;
    data.value = {
      ...data.value,
      blocks: [
        ...blocks,
        {
          left: e.offsetX,
          top: e.offsetY,
          zIndex: 1,
          key: currentComponent.key,
          alignCenter: true,
          props: {},
          model: {},
        },
      ],
    };
    currentComponent = null;
  };
  return { dragStart, dragEnd };
};

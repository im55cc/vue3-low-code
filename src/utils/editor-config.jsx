// 列表区可以显示所有的物料
// key 对应的组件映射关系

import { ElButton, ElInput } from "element-plus";

function createEditorConfig() {
  const componentList = [];
  const componentMap = {};
  return {
    componentList,
    componentMap,
    register: (component) => {
      componentList.push(component);
      componentMap[component.key] = component;
    },
  };
}

// 新建一个创建 props 的工厂函数
const createInputProps = (label) => {
  return { type: "input", label };
};
const createColorProps = (label) => {
  return { type: "color", label };
};
const createSelectProps = (label, options) => {
  return { type: "select", label, options };
};
export let registerConfig = createEditorConfig();

registerConfig.register({
  label: "文本",
  preview: () => "预览文本",
  render: ({ props }) => (
    <span style={{ color: props.color, fontSize: props.size }}>
      {props.text || "渲染文本"}
    </span>
  ),
  key: "text",
  props: {
    text: createInputProps("文本内容"),
    color: createColorProps("文字颜色"),
    size: createSelectProps("文字大小", [
      { label: "14px", value: "14px" },
      { label: "20px", value: "14px" },
      { label: "24px", value: "24px" },
    ]),
  },
});
registerConfig.register({
  label: "按钮",
  preview: () => <ElButton>预览按钮</ElButton>,
  render: ({ props }) => (
    <ElButton type={props.type} size={props.size}>
      渲染按钮
    </ElButton>
  ),
  key: "button",
  props: {
    text: createInputProps("按钮文字"),
    type: createSelectProps("按钮类型", [
      { label: "基本", value: "primary" },
      { label: "成功", value: "success" },
      { label: "警告", value: "warning" },
      { label: "危险", value: "danger" },
      { label: "文本", value: "text" },
    ]),
    size: createSelectProps("按钮大小", [
      { label: "默认", value: "" },
      { label: "大", value: "large" },
      { label: "小", value: "small" },
    ]),
  },
});
registerConfig.register({
  label: "输入框",
  preview: () => <ElInput placeholder="预览输入框"></ElInput>,
  render: ({ model }) => (
    <ElInput placeholder="渲染输入框" {...model.default}></ElInput>
  ),
  key: "input",
  model: {
    default: "绑定字段",
  },
});

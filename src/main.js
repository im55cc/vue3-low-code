import { createApp } from "vue";
import App from "./App.vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";

createApp(App).use(ElementPlus).mount("#app");

// 1. 先构造一些假数据 能实现根据位置渲染内容
// 2. 配置组件映射关系（preview:xxx,render:xxx）
